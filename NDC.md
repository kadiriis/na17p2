# Note de Clarification pour le sujet 1 - "Adhésion: Outil de gestion des adhérents d'une association"

## Description du projet
Le but du projet est de réussir à créer une base de donnée permettant principalement de gérer les adhésions de membres à une association. L'application permettra également de gérer les dons, les cotisations, et également d'assigner des utilisateurs à des groupes. Enfin, les informations relatives aux adhérents pourront être archivées par l'application.

## Liste des objets nécessaires à la modélisation
**Utilisateur**
- posséde un login et un mot de passe
- posséde un nom et prénom
- posséde une photo
- posséde un mail
- posséde ou non un numéro de mobile
- posséde ou non une adresse
- posséde une date d'inscription
- peut adhérer à 0..N associations
- peut faire des dons à 0..N associations
- peut être assignée à 0..N groupes
- peut cotiser

**Cotisation**
- posséde un montant 
- posséde une périodicité (hebdomadaire, mensuelle ou annuelle)
- posséde une date d'expiration

**Administrateur**
- gère les adhésions
- gère les dons
- gère les cotisations
- gère les groupes

**Donateur**
- réalise des dons

**Don**
- posséde un montant 
- posséde une date 

**Adherent**
- posséde une adhésion

**Adhesion**
- posséde une date d'adhésion
- posséde une date de fin d'adhésion
- posséde un montant d'adhésion
- posséde un type d'adhésion (Bronze, Or, Argent, Normal, Honoraire, Etudiant, Activite...)

**Groupe**
- posséde un nom
- posséde une page
- posséde 0..N photos
- posséde 0..N évènements
- posséde 0..N outils web
- posséde 0..N informations générales

**Photo**
- posséde un titre
- posséde une URL de fichier

**Evenement**
- posséde un intitulé
- posséde une date

**OutilWeb**
- posséde un nom
- posséde une URL

**InformationGenerale**
- posséde un titre
- posséde un texte

**archiveAdherent**
- posséde les mêmes attributs qu'un utilisateur adherent

**archiveAdhesion**
- posséde les mêmes attributs qu'une adhesion

## Contraintes du sujet
- Une personne aura le choix de publier ou non son nom, prénom et sa photo sur le site de l'association
- Une personne ne fera plus partie d'une association si la date de fin de son adhésion est dépassée
- Un donateur n'est pas forcément un adhérent
- Un adhérent n'est pas forcément un donateur
- Les informations concernant les groupes sont périssables
- Les informations concernant les adhésions et adhérents doivent pouvoir être archivées

## Liste des utilisateurs
Un utilisateur peut être :
- un adhérent
- un administrateur
- un donateur

On souhaite archiver les adhérents et adhésions. Ainsi un utilisateur qui n'est plus adhérent a toujours ses informations d'ancien adhérent stockées dans la base de données. Par conséquent un utilisateur peut ne pas avoir l'un 3 rôles cités. C'est pour cela que classe utilisateur n'est pas abstraite (elle est instanciée dans notre cas)

## Droits des utilisateurs
**Personne**
- peut modifier ses informations personnelles

**Administrateur**
- peut consulter la liste des personnes inscrites ainsi que leurs informations personnelles
- peut gérer les adhésions des personnes (date d'adhésion, montant versé)
- peut obtenir les dates d'adhésions de chaque personne
- peut obtenir les listes d'adhésions qui se terminent prochainement
- peut obtenir les listes des cotisations qui se terminent prochainement
- peut gérer les types d'adhésions
- peut gérer les donateurs et les dons
- peut créer et gérer les groupes
- peut assigner des personnes à des groupes
- peut accéder aux archives

**Personne au sein d'un groupe**
- peut ajouter des informations (date d'un événément, URL d'un outil web utilisé par le groupe, photo, information générale)

## Hypothèses émises 
- Un administrateur doit être inscrit sur le site
- Une personne n'est contrainte que de renseigner son nom, prénom et mail ainsi que d'upload une photo
- Une cotisation peut être réalisée par n'importe quel utilisateur, suivant une périodicité donnée
- Les informations relatives aux adhérents sont stockées doublement (dans les tables Adherent, Adhesion ainsi que dans les tables archiveAdherent, archiveAdhesion)