DROP TYPE IF EXISTS typeAdhesion CASCADE;
DROP TYPE IF EXISTS periodicite CASCADE;
DROP TABLE IF EXISTS Utilisateur CASCADE;
DROP TABLE IF EXISTS Cotisation CASCADE;
DROP TABLE IF EXISTS Administrateur CASCADE;
DROP TABLE IF EXISTS Adherent CASCADE;
DROP TABLE IF EXISTS Adhesion CASCADE;
DROP TABLE IF EXISTS Donateur CASCADE;
DROP TABLE IF EXISTS Don CASCADE;
DROP TABLE IF EXISTS Groupe CASCADE;
DROP TABLE IF EXISTS appartenirGroupe CASCADE;
DROP TABLE IF EXISTS InformationGenerale CASCADE;
DROP TABLE IF EXISTS OutilWeb CASCADE;
DROP TABLE IF EXISTS utiliseOutil CASCADE;
DROP TABLE IF EXISTS Evenement CASCADE;
DROP TABLE IF EXISTS organiseEvenement CASCADE;
DROP TABLE IF EXISTS Photo CASCADE;
DROP TABLE IF EXISTS archiveAdherent CASCADE;
DROP TABLE IF EXISTS archiveAdhesion CASCADE;
DROP VIEW IF EXISTS vueAdministrateur CASCADE;
DROP VIEW IF EXISTS vueAdherent CASCADE;
DROP VIEW IF EXISTS vueNonAdherent CASCADE;
DROP VIEW IF EXISTS vueAdhesionExpire CASCADE;
DROP VIEW IF EXISTS vueTypeAdhesion CASCADE;
DROP VIEW IF EXISTS vueDonateur CASCADE;
DROP VIEW IF EXISTS vueDon CASCADE;
DROP VIEW IF EXISTS vueCotisation CASCADE;
DROP VIEW IF EXISTS vueCotisationExpire CASCADE;
DROP VIEW IF EXISTS vueCotisationPeriodicite CASCADE;
DROP VIEW IF EXISTS vueGroupe CASCADE;
DROP VIEW IF EXISTS vueGroupeCle CASCADE;
DROP VIEW IF EXISTS vueGroupeActif CASCADE;
DROP VIEW IF EXISTS vueArchiveAdherent CASCADE;
 
CREATE TYPE typeAdhesion AS ENUM ('Bronze', 'Or', 'Argent', 'Normal', 'Honoraire', 'Etudiant', 'Activite');
CREATE TYPE periodicite AS ENUM ('Hebdomadaire', 'Mensuelle', 'Annuelle');

CREATE TABLE Utilisateur (
    userLogin  VARCHAR PRIMARY KEY,
    motDePasse  VARCHAR NOT NULL,
    nom  VARCHAR NOT NULL,
    prenom  VARCHAR NOT NULL,
    mail  VARCHAR NOT NULL,
    photo  VARCHAR NOT NULL,
    mobile JSON,
    adresse JSON,
    dateInscription  DATE,
    PublierInfo  CHAR(1) NOT NULL,
    CHECK (PublierInfo IN ('0', '1'))
);

CREATE TABLE Cotisation (
    idCotisation  INTEGER PRIMARY KEY,
    montantCotisation  REAL NOT NULL,
    periodicite  periodicite NOT NULL,
    dateExpiration  DATE NOT NULL,
    Cotisant VARCHAR NOT NULL,
    FOREIGN KEY (Cotisant) REFERENCES Utilisateur(userLogin)
);

CREATE TABLE Administrateur (
    loginAdmin  VARCHAR PRIMARY KEY,
    roleAdmin JSON NOT NULL,
    FOREIGN KEY (loginAdmin) REFERENCES Utilisateur(userLogin)
);

CREATE TABLE Adherent (
    loginAdherent  VARCHAR PRIMARY KEY,
    FOREIGN KEY (loginAdherent) REFERENCES Utilisateur(userLogin)
);

CREATE TABLE Adhesion (
    idAdhesion  INTEGER,
    dateAdhesion  DATE NOT NULL,
    dateFinAdhesion  DATE,
    montantAdhesion  REAL NOT NULL,
    typeAdhesion  typeAdhesion NOT NULL,
    adherent VARCHAR,
    PRIMARY KEY (idAdhesion, adherent),
    FOREIGN KEY (adherent) REFERENCES Adherent(loginAdherent),
    CHECK (dateFinAdhesion > dateAdhesion)
);


CREATE TABLE Donateur (
    loginDonateur  VARCHAR PRIMARY KEY,
    FOREIGN KEY (loginDonateur) REFERENCES Utilisateur(userLogin)
);

CREATE TABLE Don (
    idDon  INTEGER,
    montantDon  REAL NOT NULL,
    dateDon  DATE NOT NULL,
    donateur VARCHAR NOT NULL,
    PRIMARY KEY (idDon, donateur),
    FOREIGN KEY (donateur) REFERENCES Donateur(loginDonateur)
);

CREATE TABLE Groupe (
    urlPage VARCHAR PRIMARY KEY,
    dateCreation DATE NOT NULL,
    nom VARCHAR NOT NULL,
    photo JSON
);

CREATE TABLE appartenirGroupe(
    idGroupe VARCHAR,
    idUtilisateur VARCHAR,
    PRIMARY KEY (idGroupe, idUtilisateur),
    FOREIGN KEY (idGroupe) REFERENCES Groupe(urlPage),
    FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(userLogin)
);

CREATE TABLE InformationGenerale (
    idInfo  INTEGER PRIMARY KEY,
    idGroupe  VARCHAR,
    titre  VARCHAR NOT NULL,
    datePublication DATE NOT NULL,
    theme JSON,
    mention JSON,
    texte  TEXT,
    photo JSON,
    FOREIGN KEY (idGroupe) REFERENCES Groupe(urlPage)
);

CREATE TABLE OutilWeb (
    urlOutil  VARCHAR PRIMARY KEY,
    titre  VARCHAR NOT NULL,
    photo JSON
);

CREATE TABLE utiliseOutil (
    idOutil VARCHAR,
    idGroupe VARCHAR,
    PRIMARY KEY (idOutil, idGroupe),
    FOREIGN KEY (idOutil) REFERENCES OutilWeb(urlOutil),
    FOREIGN KEY (idGroupe) REFERENCES Groupe(urlPage)
);

CREATE TABLE Evenement (
    idEvenement  INTEGER PRIMARY KEY,
    intitule  VARCHAR NOT NULL,
    dateEvenement  DATE NOT NULL,
    descEvenement TEXT,
    photo JSON
);

CREATE TABLE organiseEvenement (
    idEvenement INTEGER,
    idGroupe  VARCHAR,
    PRIMARY KEY (idEvenement, idGroupe),
    FOREIGN KEY (idEvenement) REFERENCES Evenement(idEvenement),
    FOREIGN KEY (idGroupe) REFERENCES Groupe(urlPage)
);


CREATE TABLE archiveAdherent (
    userLogin  VARCHAR PRIMARY KEY,
    motDePasse  VARCHAR NOT NULL,
    nom  VARCHAR NOT NULL,
    prenom  VARCHAR NOT NULL,
    mail  VARCHAR NOT NULL,
    photo  VARCHAR NOT NULL,
    mobile  JSON,
    adresse  JSON,
    dateInscription  DATE,
    PublierInfo  CHAR(1) NOT NULL,
    adhesion JSON NOT NULL, 
    CHECK (PublierInfo IN ('0', '1'))
);

/* On gère la composition archiveAdhesion-archiveAdherent sous forme d'imbrication JSON */

/*
CREATE TABLE archiveAdhesion (
    idAdhesion  INTEGER,
    dateAdhesion  DATE NOT NULL,
    dateFinAdhesion  DATE,
    montantAdhesion  REAL NOT NULL,
    typeAdhesion typeAdhesion NOT NULL,
    adherent VARCHAR,
    PRIMARY KEY (idAdhesion, adherent),
    FOREIGN KEY (adherent) REFERENCES archiveAdherent(userLogin),
    CHECK (dateFinAdhesion > dateAdhesion)
);
*/


/* -------------------- */
/* CREATION DES VUES */

CREATE VIEW vueAdministrateur AS
    SELECT U.nom, U.prenom, U.mail, U.photo
    FROM Utilisateur U JOIN Administrateur A
    ON U.userLogin = A.loginAdmin
;

CREATE VIEW vueAdherent AS
	SELECT U.nom, U.prenom, U.photo, U.mail, AD.dateAdhesion, AD.dateFinAdhesion, AD.montantAdhesion, AD.typeAdhesion
	FROM Utilisateur U, Adherent D, Adhesion AD
    WHERE AD.dateFinAdhesion > CURRENT_TIMESTAMP
    AND AD.adherent = D.loginAdherent
    AND D.loginAdherent = U.userLogin
;

CREATE VIEW vueNonAdherent AS 
    SELECT U.nom, U.prenom, U.photo, U.mail
    FROM Utilisateur U LEFT JOIN Adherent A
    ON A.loginAdherent = U.userLogin
    WHERE A.loginAdherent IS NULL
;

CREATE VIEW vueAdhesionExpire AS
	SELECT U.nom, U.prenom, U.photo, U.mail, U.dateInscription, AD.dateFinAdhesion, AD.typeAdhesion
	FROM Utilisateur U, Adherent D, Adhesion AD
    WHERE AD.dateFinAdhesion < CURRENT_TIMESTAMP
    AND AD.adherent = D.loginAdherent
    AND D.loginAdherent = U.userLogin
;

CREATE VIEW vueTypeAdhesion AS
    SELECT A.typeAdhesion, COUNT(*) AS nbAdhesion
    FROM Adhesion A
    GROUP BY A.typeAdhesion
;

CREATE VIEW vueDonateur AS
    SELECT U.nom, U.prenom, U.mail, U.photo
    FROM Utilisateur U JOIN Donateur D
    ON U.userLogin = D.loginDonateur
;

CREATE VIEW vueDon AS
	SELECT U.nom, U.prenom, U.mail, Don.dateDon, Don.montantDon
	FROM Utilisateur U, Donateur D, Don 
    WHERE Don.donateur = D.loginDonateur
    AND D.loginDonateur = U.userLogin
;

CREATE VIEW vueCotisation AS
	SELECT U.nom, U.prenom, U.photo, U.mail, C.montantCotisation, C.periodicite, C.dateExpiration
	FROM Utilisateur U, Cotisation C
    WHERE C.Cotisant = U.userLogin
    AND C.dateExpiration > CURRENT_TIMESTAMP
;

CREATE VIEW vueCotisationExpire AS
	SELECT U.nom, U.prenom, U.photo, U.mail, C.montantCotisation, C.periodicite, C.dateExpiration
	FROM Utilisateur U, Cotisation C
    WHERE C.Cotisant = U.userLogin
    AND C.dateExpiration < CURRENT_TIMESTAMP
;

CREATE VIEW vueCotisationPeriodicite AS
    SELECT C.periodicite, COUNT(*) AS nbCotisation
    FROM Cotisation C
    GROUP BY C.periodicite
;

CREATE VIEW vueGroupe AS
    SELECT G.nom, G.urlPage, COUNT(*) AS nbMembres
    FROM Groupe G, appartenirGroupe AG, Utilisateur U
    WHERE AG.idUtilisateur = U.userLogin
    AND AG.idGroupe = G.urlPage
    GROUP BY G.nom, G.urlPage
;

CREATE VIEW vueGroupeCle AS 
    SELECT G.nom, COUNT(*) AS nbMembres
    FROM Groupe G, appartenirGroupe AG, Utilisateur U
    WHERE AG.idUtilisateur = U.userLogin
    AND AG.idGroupe = G.urlPage
    GROUP BY G.nom
    HAVING COUNT(U.userLogin) > 2
;

CREATE VIEW vueGroupeActif AS 
    SELECT G.nom, COUNT(*) AS nbMembres
    FROM Groupe G, appartenirGroupe AG, Utilisateur U, InformationGenerale IG
    WHERE AG.idUtilisateur = U.userLogin
    AND AG.idGroupe = G.urlPage
    AND IG.idGroupe = G.urlPage
    GROUP BY G.nom
    HAVING EXTRACT(YEAR FROM MAX(IG.datePublication)) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP)
;

CREATE VIEW vueArchiveAdherent AS
	SELECT A.nom, A.prenom, A.mail, A.photo, COUNT(*) AS nb_adhesion
	FROM archiveAdherent A, JSON_ARRAY_ELEMENTS(A.adhesion) s
    GROUP BY A.nom, A.prenom, A.mail, A.photo
;

/*---------------*/
/* CREATION DES INSERT */

BEGIN TRANSACTION;
INSERT INTO Utilisateur(userLogin, motDePasse, nom, prenom, mail, photo, mobile, adresse, dateInscription, PublierInfo)
VALUES
('ismailk', 'ismailk123', 'Kadiri', 'Ismail', 'ismail.kadiri@outlook.com','https://imgur.com/gallery/1kiW5ck', '["0787653462"]', '{"Ville": "Casablanca", "Pays": "Maroc"} ','2011-01-19', '0'),
('aminek', 'aminek123', 'Khazana', 'Amine', 'amine.khazana@outlook.com','https://imgur.com/gallery/1kiD6ck', NULL, NULL, '2018-02-09', '1'),
('anasm', 'anasm123', 'Masrour', 'Anas', 'anas.masrour@outlook.com','https://imgur.com/gallery/1ciM9ck', NULL, NULL, '2019-03-27', '0'),
('ayab', 'ayab123', 'Bourdaime', 'Aya', 'aya.bourdaime@outlook.com','https://imgur.com/gallery/2liW5ck', '["0683653462","0790899628"]', '{"Ville":"Syndey", "Pays":"Australie"}', '2012-01-29', '0'),
('linao', 'linao123', 'Oudghiri', 'Lina', 'lina.oudghiri@outlook.com','https://imgur.com/gallery/diW5ck', NULL, NULL, '2011-10-02', '1'),
('ninaf', 'ninaf123', 'Forde', 'Nina', 'nina.forde@outlook.com','https://imgur.com/gallery/1lpW5lk', '["0787653462","0690897628"]', '{"Ville":"Pripyat", "Pays":"Ukraine"}','2019-12-01', '0'),
('anass', 'anass123', 'Samkaoui', 'Anas', 'anas.samkaoui@outlook.com','https://imgur.com/gallery/9miW5ck', NULL, NULL, '2019-08-18', '1'),
('narjissb', 'narjissb123', 'Benbrahim', 'Narjiss', 'narjiss.benbrahim@outlook.com','https://imgur.com/gallery/1liP9ck', NULL, NULL, '2013-02-14', '0'),
('kenzaa', 'kenzaa123', 'Abdelhaq', 'Kenza', 'kenza.abdelhaq@outlook.com','https://imgur.com/gallery/1kiW5mp', NULL, NULL, '2020-05-16', '0'),
('alij', 'alij123', 'Joundi', 'Ali', 'ali.joundi@outlook.com','https://imgur.com/gallery/2kiB7lo', '["0627653462","0780897628"]', '{"Ville":"Paris", "Pays":"France"}','2019-06-07', '1');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Administrateur(loginAdmin, roleAdmin)
VALUES 
('ismailk', '[ {"Package": "Donateur", "Fonction": "Gérer dons" },  {"Package": "Groupes", "Fonction": "Gérer groupes"} ]'),
('ninaf', ' [ {"Package": "Adherent", "Fonction": "Gérer adhésions" } ]'),
('alij', '[ {"Package": "Archives", "Fonction": "Gérer archives" } ]');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Adherent(loginAdherent)
VALUES 
('ismailk'),
('anasm'),
('ayab'),
('ninaf'),
('anass'),
('kenzaa'),
('aminek'),
('alij'),
('narjissb');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Adhesion(idAdhesion, dateAdhesion, dateFinAdhesion, montantAdhesion, typeAdhesion, adherent)
VALUES 
(1, '2011-01-19', '2012-01-19', 10, 'Etudiant', 'ismailk'),
(2, '2019-03-27', '2022-03-27', 50, 'Argent', 'anasm'),
(3, '2019-06-07', '2020-06-07', 20, 'Bronze', 'alij'),
(4, '2012-01-19', '2022-01-19', 100, 'Or', 'ismailk'),
(5, '2012-01-29', '2014-01-29', 25, 'Normal', 'ayab'),
(6, '2019-12-01', '2020-12-01', 20, 'Bronze', 'ninaf'),
(7, '2019-10-01', '2021-10-01', 25, 'Normal', 'anass'),
(8, '2020-05-16', '2030-05-16', 100, 'Or', 'kenzaa'),
(9, '2019-01-01', '2022-01-01', 50, 'Argent', 'aminek'),
(10, '2020-04-20', '2025-04-20', 75, 'Activite', 'ayab'),
(11, '2013-02-14', '2015-02-14', 35, 'Honoraire', 'narjissb');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Donateur(loginDonateur)
VALUES 
('ismailk'),
('ayab'),
('aminek'),
('narjissb'),
('anass'),
('ninaf');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Don(idDon, montantDon, dateDon, donateur)
VALUES 
(1, 1000, '2011-01-30', 'ismailk'),
(2, 2500, '2012-05-18', 'ayab'),
(3, 100, '2013-02-14', 'narjissb'),
(4, 1, '2019-02-01', 'aminek'),
(5, 80, '2019-02-01', 'aminek'),
(6, 5000, '2018-10-18', 'ismailk'),
(8, 100, '2019-12-31', 'ninaf');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Cotisation(idCotisation, montantCotisation, periodicite, dateExpiration, Cotisant)
VALUES 
(1, 2500, 'Annuelle', '2025-01-01', 'ismailk'),
(2, 100, 'Mensuelle', '2019-12-31', 'ismailk'),
(3, 200, 'Mensuelle', '2022-02-01', 'ninaf'),
(4, 50, 'Hebdomadaire', '2020-03-26', 'anass'),
(5, 1000, 'Mensuelle', '2020-05-20', 'alij'),
(6, 100, 'Mensuelle', '2020-07-09', 'alij');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Groupe(urlPage, dateCreation, nom, photo)
VALUES 
('https://www.facebook.com/groups/log', '2020-01-01', 'LOGISTIQUE', '[ {"fichier":"https://imgur.com/gallery/1kiW7kc", "titre":"LOGO", "description":"Ceci est un logo bg"}, {"fichier":"https://imgur.com/gallery/1kiWc", "titre":"COUVERTURE LOG", "description":"Ceci nest pas un plaid"}]'),
('https://www.facebook.com/groups/anim', '2020-01-01', 'ANIMATION', '[ {"fichier":"https://imgur.com/gallery/1liW7kc", "titre":"LOGO", "description":"Ceci est un logo un peu moins bg"}, {"fichier":"https://imgur.com/gallery/1kiCc", "titre":"COUVERTURE ANIM"}]'),
('https://www.facebook.com/groups/sponso', '2020-01-01','SPONSO', NULL);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO appartenirGroupe(idGroupe, idUtilisateur)
VALUES 
('https://www.facebook.com/groups/log', 'anasm'),
('https://www.facebook.com/groups/log', 'anass'),
('https://www.facebook.com/groups/log', 'ismailk'),
('https://www.facebook.com/groups/anim', 'ismailk'),
('https://www.facebook.com/groups/sponso', 'ismailk'),
('https://www.facebook.com/groups/anim', 'kenzaa'),
('https://www.facebook.com/groups/anim', 'ninaf'),
('https://www.facebook.com/groups/sponso', 'ninaf'),
('https://www.facebook.com/groups/log', 'ninaf'),
('https://www.facebook.com/groups/sponso', 'alij'),
('https://www.facebook.com/groups/log', 'alij'),
('https://www.facebook.com/groups/anim', 'alij'),
('https://www.facebook.com/groups/sponso', 'aminek');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
m
(1, 'https://www.facebook.com/groups/log', 'REPARTITION DES ROLES LOG', 'Anas, Anas et Ismail', '2020-01-01', '["role", "shtroumph", "spartes"]', '["Anas", "Anas", "Ismail"]', NULL),
(2, 'https://www.facebook.com/groups/anim', 'REPARTITION DES ROLES ANIM', 'Kenza et Nina et Ismail', '2020-01-01', '["role", "pharmacie", "bouteille"]', '["Kenza", "Nina", "Ismail"]', NULL),
(3, 'https://www.facebook.com/groups/anim', 'BRAINSTORMING', 'Des idees?', '2020-01-10', '["idees", "innovation", "chômage"]', NULL , NULL),
(4, 'https://www.facebook.com/groups/log', 'TEST', 'Ca marche?', '2020-01-06', NULL, NULL, NULL),
(5, 'https://www.facebook.com/groups/sponso', 'REPARTITION DES ROLES SPONSO', 'Amine et Ali', '2019-12-31', '["role", "leonidas", "spartes"]', '["Amine", "Ali"]', NULL),
(6, 'https://www.facebook.com/groups/log', 'LOG10', 'Sah quel plaisir cette blague', '2020-01-20', NULL, NULL, '{"fichier":"https://imgur.com/gallery/1liWlmpc", "titre":"YOU GOT BAMBOOZLED", "description":"g t eu"}');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO OutilWeb(urlOutil, titre, photo)
VALUES 
('https://www.plantuml.com','PlantUML', '{"fichier":"https://imgur.com/gallery/1lidkc"}'),
('https://postgresql.org', 'PostgreSQL', '{"fichier":"https://imgur.com/gallery/1lpdkc"}'),
('https://github.com', 'GitHub', '{"fichier":"https://imgur.com/gallery/1ldk8"}'),
('https://deepl.com', 'DeepL', NULL);
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO utiliseOutil(idOutil, idGroupe)
VALUES
('https://github.com', 'https://www.facebook.com/groups/log'),
('https://www.plantuml.com', 'https://www.facebook.com/groups/log'),
('https://postgresql.org', 'https://www.facebook.com/groups/log'),
('https://github.com', 'https://www.facebook.com/groups/anim'),
('https://postgresql.org', 'https://www.facebook.com/groups/anim'),
('https://deepl.com', 'https://www.facebook.com/groups/anim'),
('https://github.com', 'https://www.facebook.com/groups/sponso'),
('https://postgresql.org', 'https://www.facebook.com/groups/sponso'),
('https://deepl.com', 'https://www.facebook.com/groups/sponso');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO Evenement(idEvenement, intitule, dateEvenement, descEvenement)
VALUES 
(1, 'TEAM BUILDING ANIM', '2020-02-10', 'TB DU FUN' ),
(2, 'TEAM BUILDING SPONSO', '2020-01-30', 'TB DES BUSINESS MEN' ),
(3, 'TEAM BUILDING LOG', '2020-02-06', 'TB DES MATHEUX'),
(4, 'TEAM BUILDING LOG 2', '2020-03-01', 'TB DES MATHEUX BOURRÉS' ),
(5, 'REUNION BRAINSTORMING ANIM 1', '2020-03-15', NULL),
(6, 'REUNION BRAINSTORMING SPONSO 1', '2020-03-10', 'BRAINSTORMONS LES ZAMIS'),
(7, 'REUNION BRAINSTORMING LOG 1', '2020-03-12', 'SPARTIATES, QUEL EST VOTRE MÉTIER?!');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO organiseEvenement(idEvenement, idGroupe)
VALUES 
(1, 'https://www.facebook.com/groups/anim'),
(2, 'https://www.facebook.com/groups/sponso'),
(3, 'https://www.facebook.com/groups/log'),
(4, 'https://www.facebook.com/groups/log'),
(5, 'https://www.facebook.com/groups/anim'),
(6, 'https://www.facebook.com/groups/sponso'),
(7, 'https://www.facebook.com/groups/log');
COMMIT TRANSACTION;

BEGIN TRANSACTION;
INSERT INTO archiveAdherent(userLogin, motDePasse, nom, prenom, mail, photo, mobile, adresse, dateInscription, PublierInfo, adhesion)
VALUES 
('ismailk', 'ismailk123', 'Kadiri', 'Ismail', 'ismail.kadiri@outlook.com','https://imgur.com/gallery/1kiW5ck', '["0787653462"]', '{"Ville": "Casablanca", "Pays": "Maroc"}','2011-01-19', '0', '[{"idAdhesion": 1,"dateAdhesion":"2011-01-09", "dateFinAdhesion":"2012-01-19","montantAdhesion": 10,"typeAdhesion":"Etudiant"},{"idAdhesion": 4,"dateAdhesion":"2012-01-19","dateFinAdhesion":"2022-01-19","montantAdhesion": 100,"typeAdhesion":"Or"}]'),
('aminek', 'aminek123', 'Khazana', 'Amine', 'amine.khazana@outlook.com','https://imgur.com/gallery/1kiD6ck', NULL, NULL, '2018-02-09', '1', '[{"idAdhesion":9,"dateAdhesion":"2019-01-01","dateFinAdhesion":"2022-01-01","montantAdhesion":50,"typeAdhesion":"Argent"}]'),
('anasm', 'anasm123', 'Masrourt', 'Anas', 'anas.masrour@outlook.com','https://imgur.com/gallery/1ciM9ck', NULL, NULL, '2019-03-27', '0', '[{"idAdhesion":2,"dateAdhesion":"2019-03-27", "dateFinAdhesion":"2022-03-27", "montantAdhesion":50,"typeAdhesion":"Argent"}]'),
('ayab', 'ayab123', 'Bourdaime', 'Aya', 'aya.bourdaime@outlook.com','https://imgur.com/gallery/2liW5ck', '["0683653462","0790899628"]', '{"Ville":"Syndey", "Pays":"Australie"}', '2012-01-29', '0', '[{"idAdhesion":5,"dateAdhesion":"2012-01-29", "dateFinAdhesion":"2014-01-29","montantAdhesion": 25,"typeAdhesion":"Normal"}, {"idAdhesion": 10,"dateAdhesion":"2020-04-20", "dateFinAdhesion":"2025-04-20","montantAdhesion": 75,"typeAdhesion":"Activite"}]'),
('ninaf', 'ninaf123', 'Forde', 'Nina', 'nina.forde@outlook.com','https://imgur.com/gallery/1lpW5lk', '["0787653462","0690897628"]', '{"Ville":"Pripyat", "Pays":"Ukraine"}','2019-12-01', '0', '[{"idAdhesion":6,"dateAdhesion":"2019-12-01", "dateFinAdhesion":"2020-12-01","montantAdhesion": 20,"typeAdhesion":"Bronze"}]'),
('anass', 'anass123', 'Samkaoui', 'Anas', 'anas.samkaoui@outlook.com','https://imgur.com/gallery/9miW5ck', NULL, NULL, '2019-08-18', '1', '[{"idAdhesion":7,"dateAdhesion":"2019-10-01", "dateFinAdhesion":"2021-10-01","montantAdhesion":25,"typeAdhesion":"Normal"}]'),
('narjissb', 'narjissb123', 'Benbrahim', 'Narjiss', 'narjiss.benbrahim@outlook.com','https://imgur.com/gallery/1liP9ck', NULL, NULL, '2013-02-14', '0', '[{"idAdhesion":11,"dateAdhesion":"2013-02-14","dateFinAdhesion":"2015-02-14","montantAdhesion": 35,"typeAdhesion":"Honoraire"}]'),
('kenzaa', 'kenzaa123', 'Abdelhaq', 'Kenza', 'kenza.abdelhaq@outlook.com','https://imgur.com/gallery/1kiW5mp', NULL, NULL, '2020-05-16', '0', '[{"idAdhesion":8,"dateAdhesion":"2020-05-16","dateFinAdhesion":"2030-05-16","montantAdhesion": 100,"typeAdhesion":"Or"}]'),
('alij', 'alij123', 'Joundi', 'Ali', 'ali.joundi@outlook.com','https://imgur.com/gallery/2kiB7lo', '["0627653462","0780897628"]', '{"Ville":"Paris", "Pays":"France"}','2019-06-07', '1', '[{"idAdhesion":3,"dateAdhesion":"2019-06-07","dateFinAdhesion":"2020-06-07","montantAdhesion":20,"typeAdhesion":"Bronze"}]');
COMMIT TRANSACTION;

/* 
CREATE USER ismailk WITH PASSWORD 'ismailk123' ;
CREATE USER ninaf WITH PASSWORD 'ninaf123' ;
CREATE USER alij WITH PASSWORD 'alij123' ;
CREATE USER aminek WITH PASSWORD 'aminek123' ;
CREATE USER anasm WITH PASSWORD 'anasm123' ;
CREATE USER ayab WITH PASSWORD 'ayab123' ;
CREATE USER linao WITH PASSWORD 'linao123' ;
CREATE USER anass WITH PASSWORD 'anass123' ;
CREATE USER narjissb WITH PASSWORD 'narjissb123' ;
CREATE USER kenzaa WITH PASSWORD 'kenzaa123' ;

CREATE GROUP anim WITH USER kenzaa, ninaf, ismailk, alij ;
CREATE GROUP sponso WITH USER aminek, ninaf, ismailk, alij ;
CREATE GROUP log WITH USER anass, anasm, ninaf, ismailk, alij ;
CREATE GROUP administrateur WITH USER ninaf, ismailk, alij ;



GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO administrateur;
GRANT SELECT ON vueAdministrateur, vueAdherent, vueDon, vueCotisation, vueGroupe TO PUBLIC;
GRANT SELECT, UPDATE, DELETE ON InformationGenerale, OutilWeb, Evenement, Photo TO anim, sponso, log;
*/ 

/* --------------------*/
/* CREATION DES SELECT */

/* vueAdministrateur */
SELECT * FROM vueAdministrateur;
/* vueAdherent */
SELECT * FROM vueAdherent;
/* vueTypeAdhesion */
SELECT * FROM vueTypeAdhesion;
/* vueAdhesionExpire  */
SELECT * FROM vueAdhesionExpire;
/* vueNonAdherent */
SELECT * FROM vueNonAdherent;
/* vueCotisation */
SELECT * FROM vueCotisation;
/* vueCotisationExpire */
SELECT * FROM vueCotisationExpire;
/* vueCotisationPeriodicite */
SELECT * FROM vueCotisationPeriodicite;
/* vueDonateur */
SELECT * FROM vueDonateur;
/* vueDon */
SELECT * FROM vueDon;
/* vueGroupe */
SELECT * FROM vueGroupe;
/* vueGroupeActif */
SELECT * FROM vueGroupeActif;
/* vueGroupeCle */
SELECT * FROM vueGroupeCle;
/* vueArchiveAdherent */
SELECT * FROM vueArchiveAdherent;

/* Retourner les utilisateurs à la fois adhérents et donateurs */

SELECT U.nom, U.prenom, U.mail 
FROM Utilisateur U 
JOIN Adherent A
ON A.loginAdherent = U.userLogin 
JOIN Donateur D 
ON D.loginDonateur = U.userLogin
ORDER BY U.nom 
;

/* Retourner le nombre d'adhésions des utilisateurs */

SELECT U.nom, U.prenom, U.mail, COUNT(*) AS nbAdhesion
FROM Utilisateur U, Adherent A, Adhesion AD
WHERE A.loginAdherent = U.userLogin
AND AD.adherent = A.loginAdherent
GROUP BY U.nom, U.prenom, U.mail
ORDER BY U.nom
;

/* Retourner les utilisateurs ayant adhéré plus d'une fois */

SELECT U.nom, U.prenom, U.mail, COUNT(*) AS nbAdhesion
FROM Utilisateur U, Adherent A, Adhesion AD
WHERE A.loginAdherent = U.userLogin
AND AD.adherent = A.loginAdherent
GROUP BY U.nom, U.prenom, U.mail
HAVING COUNT(*) > 1
ORDER BY U.nom
;

/* Retourner les utilisateurs appartenant à plusieurs groupes différents */

SELECT U.nom, U.prenom, U.mail, COUNT(*) AS nbGroupe
FROM Utilisateur U, Groupe G, appartenirGroupe AG
WHERE AG.idUtilisateur = U.userLogin
AND AG.idGroupe = G.urlPage
GROUP BY U.nom, U.prenom, U.mail
HAVING COUNT(*) > 1
ORDER BY U.nom
;

/* Retourner les groupes auquels appartient un utilisateur (par exemple les deux Anas) */

SELECT U.nom, U.prenom, U.mail, G.nom AS nomGroupe, G.urlPage
FROM Utilisateur U, Groupe G, appartenirGroupe AG
WHERE AG.idUtilisateur = U.userLogin
AND AG.idGroupe = G.urlPage
AND U.prenom = 'Anas'
;

/* Retourner les utilisateurs ayant accepté de publier leurs informations sur la page de l'association */

SELECT U.nom, U.prenom, U.mail, U.photo, U.dateInscription 
FROM Utilisateur U 
WHERE PublierInfo = '1'
ORDER BY U.nom
;

/* Retourner les utilisateurs appartenant à la team animation de l'association */

SELECT U.nom, U.prenom, U.mail, U.photo, G.nom AS nomGroupe
FROM Utilisateur U, Groupe G, appartenirGroupe AG
WHERE AG.idUtilisateur = U.userLogin
AND AG.idGroupe = G.urlPage
AND G.nom = 'ANIMATION'
ORDER BY U.nom
;

/* Retourner les administrateurs adhérents et donateurs */

SELECT U.nom, U.prenom, U.mail 
FROM Utilisateur U 
JOIN Adherent A
ON A.loginAdherent = U.userLogin 
JOIN Donateur D 
ON D.loginDonateur = U.userLogin
JOIN Administrateur ADM 
ON ADM.loginAdmin = U.userLogin
ORDER BY U.nom 
;

/* Retourner le donateur ayant réalisé le don le plus grand */

SELECT D.montantDon , D.dateDon, U.nom, U.prenom, U.mail
FROM Utilisateur U, Donateur, Don D
WHERE U.userLogin = Donateur.loginDonateur
AND Donateur.loginDonateur = D.donateur
AND D.montantDon = ( SELECT MAX (montantDon) FROM Don )
;

/* Retourner les adhérents ayant payé le plus grand montant d'adhésion */

SELECT AD.montantAdhesion, AD.dateAdhesion, AD.dateFinAdhesion, U.nom, U.prenom, U.mail
FROM Utilisateur U, Adherent A, Adhesion AD
WHERE U.userLogin = A.loginAdherent
AND A.loginAdherent = AD.adherent
AND AD.montantAdhesion = ( SELECT MAX (montantAdhesion) FROM Adhesion )
;

/* Retourner les cotisations qui vont expirer dans le mois qui vient */

SELECT U.nom, U.prenom, C.montantCotisation, C.periodicite, C.dateExpiration
FROM Utilisateur U, Cotisation C
WHERE C.Cotisant = U.userLogin 
AND EXTRACT(MONTH FROM (CURRENT_TIMESTAMP + interval '1 month'))= EXTRACT( MONTH FROM C.dateExpiration )
;

/* Retourner les utilisateurs ayant renseigné leur numéro de téléphone */
SELECT U.nom, U.prenom, U.mail, m.*
FROM Utilisateur U,  JSON_ARRAY_ELEMENTS(U.mobile) m
ORDER BY U.nom
;

/* Retourner la ville des utilisateurs ayant renseigné leur adresse */
SELECT U.nom, U.prenom, U.mail, U.adresse->>'Ville' AS VILLE
FROM Utilisateur U
WHERE U.adresse->>'Ville' IS NOT NULL
ORDER BY U.nom
;

/* Retourner l'URL des logos des groupes */
SELECT p->>'fichier' AS url_photo, p->>'description' AS description_photo
FROM Groupe G, JSON_ARRAY_ELEMENTS(G.photo) p
WHERE p->>'titre' = 'LOGO'
;

/* Retourner l'administrateur responsable du package Donateur */
SELECT U.nom, U.prenom, U.mail, r->>'Package' AS package_géré
FROM Utilisateur U, Administrateur A, JSON_ARRAY_ELEMENTS(A.roleAdmin) r 
WHERE U.userLogin = A.loginAdmin
AND r->>'Package' = 'Donateur'
;

/* Retourner les informations relatives au thème 'spartes' et mentionnant l'utilisateur Ismail */
SELECT I.titre, I.datePublication, t AS theme, m AS mention
FROM InformationGenerale I, JSON_ARRAY_ELEMENTS_TEXT(I.theme) t, JSON_ARRAY_ELEMENTS_TEXT(I.mention) m
WHERE t = 'spartes'
AND m = 'Ismail'
;