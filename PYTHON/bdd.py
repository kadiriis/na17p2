#!/usr/bin/env python
# coding: utf-8

# In[138]:


import psycopg2
import json
from datetime import datetime


# In[154]:


#remarque: j'ai essayé de me connecter au serveur de l'utc, mais je pense avoir un problème de VPN bien qu'il marche normalement, je me suis donc rabattu sur des tests sur le serveur local

HOST = "localhost"
USER = "me"
PASSWORD = "secret"
DATABASE = "mydb"

# Connect to an existing database
try:
  conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
except psycopg2.OperationalError as e:
  print ("I am unable to connect to the database")
  raise(e)
    
#remarque: la bdd est supposée déjà contenir toutes les tables 


# In[110]:


# Open a cursor to send SQL commands
cur = conn.cursor()

# Execute a SQL INSERT command
sql = "SELECT G.nom, G.urlPage, I.titre, I.datePublication, I.theme, I.mention, I.texte, I.photo FROM InformationGenerale I, Groupe G WHERE I.idGroupe = G.urlPage"
cur.execute(sql)

# Commit (transactionnal mode is by default)
conn.commit()

# Fetch data line by line
raw = cur.fetchone()
while raw:
    print (f"Groupe: {raw[0]}")
    print (f"Page du groupe: {raw[1]}")
    print (f"Titre: {raw[2]}, publié le {raw[3]}")
    if raw[4]:
        print("Thème(s): ")
        for i in range(len(raw[4])):
            print (raw[4][i])
    else: 
        print("ATTENTION: Pas de thème enregistré.")
        
    if raw[5]: 
        print("Personne(s) mentionnée(s): ")
        for i in range (len(raw[5])):
             print (raw[5][i])
    else: 
        print("=> Aucune personne mentionnée.")
    if raw[6]:
        print(f"Texte: {raw[6]}")
    else: 
        print("Aucune texte saisi.")
    if raw[7]:
        print(f"Photo(s) jointe(s): {json.dumps(raw[7], indent = 4)}")
    else: 
        print("Aucune photo jointe.")
    raw = cur.fetchone()
    print("\n")


# In[ ]:


choice = '1'

while choice in ['1','2','3']:
    
    print ("Pour afficher les informations relatives à un thème, entrez 1")
    print ("Pour afficher les informations mentionnant une personne, entrez 2")
    print ("Pour ajouter une information, entrez 3")
    print ("Pour quitter, entrez n'importe quel autre chiffre")
    print ("Votre saisie: ")
    
    choice = input()  
    
    if choice == '1':
        
        theme = input("Choisissez le thème désiré :") #exemple: 'spartes'
        print("\n")
        cur = conn.cursor()
        sql = "SELECT G.nom, G.urlPage, I.titre, I.datePublication, I.theme, I.mention, I.texte, I.photo FROM InformationGenerale I, Groupe G WHERE I.idGroupe = G.urlPage "
        cur.execute(sql)
        conn.commit()
        raw = cur.fetchone()
        drapeau = 0
        
        while raw:
            
            if raw[4]:
                if theme in raw[4]:
                    drapeau = 1
                    print (f"Groupe: {raw[0]}")
                    print (f"Page du groupe: {raw[1]}")
                    print (f"Titre: {raw[2]}, publié le {raw[3]}")
                    if raw[4]:
                        print("Thème(s): ")
                        for i in range(len(raw[4])):
                            print (raw[4][i])

                    if raw[5]: 
                        print("Personne(s) mentionnée(s): ")
                        for i in range (len(raw[5])):
                             print (raw[5][i])
                    else: 
                        print("=> Aucune personne mentionnée.")
                    if raw[6]:
                        print(f"Texte: {raw[6]}")
                    else: 
                        print("Aucune texte saisi.")
                    if raw[7]:
                        print(f"Photo(s) jointe(s): {json.dumps(raw[7], indent = 4)}")
                    else: 
                        print("Aucune photo jointe.")
                    print("\n")
                    
            raw = cur.fetchone()
            
        if drapeau == 0:
            print("Aucune information relative à ce thème\n")
            
    if choice == '2':
        
        personne = input("Choisissez la personne concernée :") #exemple: 'Ismail'
        print("\n")
        
        cur = conn.cursor()
        sql = "SELECT G.nom, G.urlPage, I.titre, I.datePublication, I.theme, I.mention, I.texte, I.photo FROM InformationGenerale I, Groupe G WHERE I.idGroupe = G.urlPage "
        cur.execute(sql)
        conn.commit()
        raw = cur.fetchone()
        drapeau = 0
        
        while raw:
            
            if raw[5]:
                
                if personne in raw[5]:
                    drapeau = 1
                    print (f"Groupe: {raw[0]}")
                    print (f"Page du groupe: {raw[1]}")
                    print (f"Titre: {raw[2]}, publié le {raw[3]}")
                    if raw[4]:
                        print("Thème(s): ")
                        for i in range(len(raw[4])):
                            print (raw[4][i])
                    else: 
                        print("ATTENTION: Pas de thème enregistré.")

                    if raw[5]: 
                        print("Personne(s) mentionnée(s): ")
                        for i in range (len(raw[5])):
                             print (raw[5][i])
                    if raw[6]:
                        print(f"Texte: {raw[6]}")
                    else: 
                        print("Aucune texte saisi.")
                    if raw[7]:
                        print(f"Photo(s) jointe(s): {json.dumps(raw[7], indent = 4)}")
                    else: 
                        print("Aucune photo jointe.")
                    print("\n")
                    
            raw = cur.fetchone()
            
        if drapeau == 0:
            print("Aucune information mentionnant cette personne\n")
            
    if choice == '3':
        
        idInfo = int(input("Entrez l'id de l'information! : "))
        while not idInfo:
            idInfo = int(input("ATTENTION: l'ID est obligatoire.\nEntrez l'id de l'information: "))
            
        idGroupe = input("Entrez l'URL du groupe concerné par l'information! :")
        while not idGroupe:
            idGroupe = input("ATTENTION: l'URL est obligatoire.\nEntrez l'URL du groupe concerné par l'information: ")
            
        titre = input("Entrez le titre de l'information! :")
        while not titre:
            titre = input("ATTENTION: le titre est obligatoire.\nEntrez le titre de l'information! :")
            
        theme = []
        t = input("Saisir un ou plusieurs thèmes: ")
        while t:
            theme.append(f"{t}")
            t = input("Saisir un ou plusieurs thèmes")
        theme = json.dumps(theme)

        personne = []
        p = input("Saisir une ou plusieurs personnes mentionnées: ")
        while p:
            personne.append(f"{p}")
            p = input("Saisir une ou plusieurs personnes mentionnées: ")
        personne = json.dumps(personne)
            
        texte = input("Saisir le texte: ")
        if not texte:
            texte = "NULL".strip('""')
        
        ph = int(input("Si vous souhaitez inclure un fichier image, entrez 0, sinon entrez 1: "))
        if ph == 0:
            fichier = input("Saisir l'URL du fichier: ")
            titre = input("Entrez un titre : ")
            description = input("Entrez une description :")
            photo = {
                "fichier" : fichier,
                "titre" : titre,
                "description" : description
            }
            photo = f"'{json.dumps(photo)}'"
        else: 
            photo = 'NULL'
            
        cur = conn.cursor()
        sql = f"INSERT INTO InformationGenerale(idInfo, idGroupe, titre, texte, datePublication, theme, mention, photo) VALUES ({idInfo}, '{idGroupe}', '{titre}', '{texte}', '{str(datetime.now())}', '{theme}', '{personne}', {photo})"
        cur.execute(sql)
        conn.commit()
        print("\n")


# In[153]:


# Close connection
conn.close()
print (conn)


# In[ ]:




