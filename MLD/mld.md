Utilisateur (#login  string, motDePasse  string, nom  string, prenom  string, mail  string, photo  URL, mobile  JSON, adresse  JSON, dateInscription  date, PublierInfo  char(1)) avec motDePasse  UNIQUE et NOT NULL, mail UNIQUE, nom et prenom et mail et photo NOT NULL

Cotisation (#idCotisation  integer, montantCotisation  float, periodicite  {Hebdomadaire | Mensuelle | Annuelle}, dateExpiration  date, Cotisant => Utilisateur) avec montantCotisation et dateExpiration et Cotisant  NOT NULL 

Choix d'un héritage par référence (la classe Utilisateur est non abstraite dans mon cas, ce choix est donc préférable) 

Administrateur (#loginAdmin=>Utilisateur, role JSON) avec role NOT NULL

Adherent (#loginAdherent=>Utilisateur)

Adhesion (#idAdhesion  integer, dateAdhesion  date, dateFinAdhesion  date, montantAdhesion  float, typeAdhesion  {Bronze | Or | Argent | Normal | Honoraire | Etudiant | Activite}, #adherent=>Adherent) avec dateFinAdhesion et dateAdhesion et typeAdhesion et montantAdhesion NOT NULL

Donateur (#loginDonateur=>Utilisateur)

Don (#idDon  integer, montantDon  float, dateDon  date, #donateur=>Donateur) avec montantDon et dateDon NOT NULL

Groupe (#urlPage  string, nom  string, dateCreation) avec nom UNIQUE et dateCreation NOT NULL

appartenirGroupe (#groupe=>Groupe, #utilisateur=>Utilisateur) 

InformationGenerale (#idInfo  integer, titre  string, datePublication DATE, theme JSON, mention JSON, texte text) avec titre et datePublication NOT NULL

OutilWeb (#urlOutil  string, titre  string) avec titre NOT NULL

utiliseOutil (#outil=>OutilWeb, #groupe=>Groupe)

Evenement (#idEvenement  integer, intitule  string, description text, dateEvenement  date) avec intitule et date NOT NULL

organiseEvenement (#groupe=>Groupe, #evenement=>Evenement)

Photo (#urlFichier  string, titre string, description text, groupe=>Groupe, info=>InformationGenerale, outil=>OutilWeb, evenement=>Evenement)

archiveAdherent (#login  string, motDePasse  string, nom  string, prenom  string, mail  string, photo  URL, mobile JSON, adresse JSON, dateInscription  date, PublierInfo  char(1), adhesion JSON) avec motDePasse  UNIQUE et NOT NULL, mail UNIQUE, nom et prenom et mail et photo et adhesion NOT NULL
